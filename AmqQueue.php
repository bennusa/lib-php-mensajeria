<?php
namespace Lib\Jms;

require_once("/config/config.php");
require_once("/lib/stomp/Stomp.php");
require_once("/lib/stomp/Stomp/Message.php");

// Gestión de colas AMQ

class AmqQueue {

	private $con;
	private $name;

	public function init($config) {
		$this->con = new \Stomp($GLOBALS['CONFIG'][$config]['url']);
		$this->con->connect($GLOBALS['CONFIG'][$config]['user'], $GLOBALS['CONFIG'][$config]['pass']);
		$this->name = $GLOBALS['CONFIG'][$config]['name'];
	}

	public function finish() {
		$this->con->disconnect();
	}

	public function sendMessage($object, $jsonEncode = true, $delaySeconds = 0) {
		$header = array();
		$header['persistent'] = 'true';
		if ($jsonEncode) {
			$header['transformation'] = 'jms-json-object';
		}
		if ($delaySeconds > 0) {
			$header['AMQ_SCHEDULED_DELAY'] = $delaySeconds * 1000;
		}
		$message = new \StompMessage ($jsonEncode ? json_encode($object) : $object, $header);
		$this->con->send($this->name, $message);
	}

	public function receiveMessage() {
		$message = $this->con->readFrame();
		if ($message !== false) {
			$this->con->ack($message);
			return $message;
		} else {
			return null;
		}
	}
}

?>
