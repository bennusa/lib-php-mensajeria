<?php
namespace Lib\Jms;

require_once("/config/config.php");
require_once("/lib/stomp/Stomp.php");
require_once("/lib/stomp/Stomp/Message.php");
//Cargo la libreria SQS de aws
require_once("/lib/aws/aws-autoloader.php");

// Gestión de colas SQS

class SqsQueue {

	private $sqsClient;
	private $queueUrl;

	public function init($config) {
		$this->sqsClient = \Aws\Sqs\SqsClient::factory(array(
			'credentials' => array(
				'key' => $GLOBALS['CONFIG'][$config]['key'],
				'secret' => $GLOBALS['CONFIG'][$config]['secret'],
			),
			'region' => $GLOBALS['CONFIG'][$config]['region'],
			'version' => 'latest',
		));
		$this->queueUrl = $GLOBALS['CONFIG'][$config]['queueUrl'];
	}

	public function sendMessage($object, $jsonEncode = true, $delaySeconds = 0) {
		$this->sqsClient->sendMessage(array(
			'QueueUrl' => $this->queueUrl,
			'MessageBody' => $jsonEncode ? json_encode($object) : $object,
			'DelaySeconds' => $delaySeconds,
		));
	}

	public function receiveMessage() {
		$result = $this->sqsClient->receiveMessage(array(
			'QueueUrl' => $this->queueUrl
		));
		$messages = $result->get('Messages');
		if (isset($messages)) {
			return $messages;
		} else {
			return array();
		}
	}

	public function deleteMessage($message) {
		$this->sqsClient->deleteMessage(array(
			'QueueUrl' => $this->queueUrl,
			'ReceiptHandle' => $message["ReceiptHandle"],
		));
	}
}

?>
