<?php
namespace Lib\Jms;

require_once("/config/config.php");
require_once("/lib/stomp/Stomp.php");
require_once("/lib/stomp/Stomp/Message.php");


class JmsQueue {

	private $broker;
	private $amqQueue;
	private $sqsQueue;

	public function init($config) {

		$this->broker = $GLOBALS['CONFIG'][$config]['broker'];

		if ($this->broker == 'AMQ') {
			$this->amqQueue = new AmqQueue();
			$this->amqQueue->init("amq-$config");
		} else if ($this->broker == 'SQS') {
			$this->sqsQueue = new SqsQueue();
			$this->sqsQueue->init("sqs-$config");
		} else {
			// Error
		}
	}

	public function finish() {
		if ($this->broker == 'AMQ') {
			$this->amqQueue->finish();
		}
	}

	public function sendMessage($object, $jsonEncode = true, $delaySeconds = 0) {
		if ($this->broker == 'AMQ') {
			$this->amqQueue->sendMessage($object, $jsonEncode, $delaySeconds);
		} else if ($this->broker == 'SQS') {
			$this->sqsQueue->sendMessage($object, $jsonEncode, $delaySeconds);
		} else {
			// Error
		}
	}

	public function receiveMessage() {
		if ($this->broker == 'AMQ') {
			$this->amqQueue->receiveMessage();
		} else if ($this->broker == 'SQS') {
			$this->sqsQueue->receiveMessage();
		} else {
			// Error
		}
	}
}

?>
